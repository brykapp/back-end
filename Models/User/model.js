const mongoose = require('mongoose');

let userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    password: {
        type: String
    },
    createdAt: {
        type: Date,
        default: Date.now()
    }
})

var user = mongoose.model('User', userSchema);

module.exports = user;
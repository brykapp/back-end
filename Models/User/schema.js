const { gql } = require('apollo-server-express');

const schema = gql`
type User {
  id: String!
  name: String!
  password: String!
}

type UserQuery {
  id: String!
  name: String!
  password: String!
}


input SignupInput {
  name: String!
  password: String!
}

input UpdateUserInput {
  name: String
  password: String
  email: String
}

type Mutation {
  createUser(params: SignupInput): UserQuery!
  deleteUser(id: String): User!
  updateUser(id: String!, params: UpdateUserInput): User!
}

type Subscription {
  demoSubscription: UserQuery
}

type Query {
  fetchCurrentUser: UserQuery!
  fetchUsers: [User]!
  fetchUser(id: String!): User!
}

`;

module.exports = schema;
const { ApolloError } = require('apollo-server');

const User = require('./model');


// Create New User
exports.createNewUser = async (params) => {
    // await Helpers.checkSuperAdmin(context);
  return new Promise((resolve, reject) => {
    const { name,password } = params
    const user = {
     name:name,
     password:password
    };
  
    // Create new user
    const newUser = new User(user);
    newUser.save()
  
    // Resolve New user
    resolve(user);
  })
  }
  

// Fetch Current Logged In User by Token
exports.fetchCurrentUser =  function(context) {
    return new Promise((resolve, reject) => {
        if (!context.user) {
             reject(new ApolloError("Login Required"));
        }

        resolve(context.user);
    })
}


// Delete User
exports.deleteUser = async (id, params) => {
    return new Promise((resolve, reject) => {
        User.findByIdAndRemove({_id: id}, (err, user) => {
            resolve(user)
        })
    })
}

// Update User
exports.updateUser = async (id, params) => {
    return new Promise((resolve, reject) => {
        User.findOneAndUpdate({_id: id}, { $set : params }, (err, user) => {
            resolve(user)
        })
    })
}

// Fetch Users
exports.fetchUsers = async (params, context) => {
    return new Promise((resolve, reject) => {
        // if (!context.user) {
            //     reject(new ApolloError("Login Required"));
            // }
            User.find({}, (err, user) => {
                resolve(user)
            }).populate('school role').exec()
    })
}

// Fetch User

exports.fetchUser = async (input) => {
    return new Promise((resolve, reject) => {
        User.findById(input.id, (err, user) => {
            resolve(user)
        }).populate('school role')
    })
}


const routes = require('express').Router();
const Methods = require('../../Resolvers/imports');

routes.post('/', (req, res) => {

  const user = Methods.User.fetchUsers(req.body)
  res.status(200).json({ message: user });
});

// POST User Login
routes.post('/users/login', async (req, res) => {
  console.log("Request Body", req.body);
  let user = await Methods.User.loginUser(req.body);

  console.log('from route ',req.body)
  let statusCode = 201
  console.log("Request Body", req.body);
  res.status(statusCode).json({ 
    status: "Success", 
    code: statusCode, 
    data: { 
      user
    }
  });
});

// testing
routes.post('/users/list', async (req, res) => {
  
  let user = await Methods.User.fetchUsers(req.body);

  console.log('from route ',req.body)
  let statusCode = 201
  console.log("Request Body", req.body);
  res.status(statusCode).json({ 
    status: "Created", 
    code: statusCode, 
    data: { 
      user 
    }
  });
}); 
// POST User Create
routes.post('/users/create', async (req, res) => {
  
  let user = await Methods.User.createNewUser(req.body);

  console.log('from route ',req.body)
  let statusCode = 201
  console.log("Request Body", req.body);
  res.status(statusCode).json({ 
    status: "Created", 
    code: statusCode, 
    data: { 
      user 
    }
  });
});


module.exports = routes;

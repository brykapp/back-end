
const { ApolloError, PubSub } = require('apollo-server');
const Database = require('../Database/db');
const Methods = require('./imports'); // Mongoose Models Imports
const pubsub = new PubSub();





// Connect to Database
Database.connectToDB()

const DEMO_LOGGEDIN = 'DEMO_LOGGEDIN';


const resolvers = {
    Subscription: {
        demoSubscription: {
            subscribe: () => pubsub.asyncIterator([DEMO_LOGGEDIN]),
        }, 
    },
    Query: {
        // User.
        fetchCurrentUser: async (rootValue, input, context) => {
            // Subscription Notification for Real-Time
            pubsub.publish(DEMO_LOGGEDIN, { demoSubscription: Methods.User.fetchCurrentUser(context) });
            // Query Method
            return await Methods.User.fetchCurrentUser(context);
        },
        fetchUsers: async (params, context) => {
            return await Methods.User.fetchUsers(params, context)
        },
        fetchUser: async (_,id) => {
            return await Methods.User.fetchUser(id)
        },

    },
       

    Mutation: {
        createUser: async (_, {params}, context) => {
            return await Methods.User.createNewUser(params);
        },
        deleteUser: async (_, {id, params}, context) => {
            return await Methods.User.deleteUser(id, params)
        },
        updateUser: async (_, {id, params}, context) => {
            return await Methods.User.updateUser(id, params)
        },
    }}
module.exports = resolvers;
